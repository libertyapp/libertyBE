-- phpMyAdmin SQL Dump
-- version 4.7.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 28, 2017 at 10:21 AM
-- Server version: 5.5.52-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dev_hathl_liberty`
--

-- --------------------------------------------------------

--
-- Table structure for table `import_log`
--

CREATE TABLE `import_log` (
  `id` int(11) NOT NULL,
  `start` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `finish` timestamp NULL DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mobility_api`
--

CREATE TABLE `mobility_api` (
  `id` int(11) NOT NULL,
  `id_import` int(11) DEFAULT NULL,
  `id_from` int(11) NOT NULL,
  `id_to` int(11) NOT NULL,
  `uniques` int(11) DEFAULT NULL,
  `fromType` int(11) DEFAULT NULL,
  `toType` int(11) DEFAULT NULL,
  `count` int(11) NOT NULL,
  `distance` double NOT NULL DEFAULT '9999.9'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mobility_api_plan`
--

CREATE TABLE `mobility_api_plan` (
  `id` int(11) NOT NULL,
  `id_import` int(11) NOT NULL,
  `id_from` int(11) NOT NULL,
  `id_to` int(11) NOT NULL,
  `uniques` int(11) DEFAULT NULL,
  `fromType` int(11) DEFAULT NULL,
  `toType` int(11) DEFAULT NULL,
  `finished` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mobility_api_syshist`
--

CREATE TABLE `mobility_api_syshist` (
  `id_hist` int(11) NOT NULL,
  `oper` char(1) NOT NULL,
  `account` varchar(100) NOT NULL,
  `event_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id` int(11) NOT NULL DEFAULT '0',
  `id_import` int(11) DEFAULT NULL,
  `id_from` int(11) NOT NULL,
  `id_to` int(11) NOT NULL,
  `uniques` int(11) DEFAULT NULL,
  `fromType` int(11) DEFAULT NULL,
  `toType` int(11) DEFAULT NULL,
  `count` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `effective_from` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `effective_to` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status_flag` varchar(1) DEFAULT '0',
  `sysaccount_created` varchar(100) DEFAULT NULL,
  `sysaccount_updated` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `pivot_age_analysis`
-- (See below for the actual view)
--
CREATE TABLE `pivot_age_analysis` (
`region_id` int(11)
,`region_name` varchar(255)
,`lon` varchar(255)
,`lat` varchar(255)
,`AGE_8_18` decimal(32,0)
,`AGE_19_25` decimal(32,0)
,`AGE_26_35` decimal(32,0)
,`AGE_36_55` decimal(32,0)
,`AGE_56_PLUS` decimal(32,0)
,`ALL_TRANZIT` decimal(32,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `pivot_gender_analysis`
-- (See below for the actual view)
--
CREATE TABLE `pivot_gender_analysis` (
`region_id` int(11)
,`region_name` varchar(255)
,`lon` varchar(255)
,`lat` varchar(255)
,`MALE_VISIT` decimal(32,0)
,`FEMALE_VISIT` decimal(32,0)
,`MALE_TRANZIT` decimal(32,0)
,`FEMALE_TRANZIT` decimal(32,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `pivot_mobility_analysis`
-- (See below for the actual view)
--
CREATE TABLE `pivot_mobility_analysis` (
`region_id` int(11)
,`region_name` varchar(255)
,`lon` varchar(255)
,`lat` varchar(255)
,`TO_UNIQUE_VISITS` decimal(32,0)
,`TO_UNIQUE_TRANZITS` decimal(32,0)
,`TO_UNIQUE_BOTH` decimal(32,0)
);

-- --------------------------------------------------------

--
-- Table structure for table `socio_age_api`
--

CREATE TABLE `socio_age_api` (
  `id` int(11) NOT NULL,
  `id_import` int(11) DEFAULT NULL,
  `id_from` int(11) NOT NULL,
  `ageGroup` int(11) NOT NULL,
  `occurenceType` int(11) NOT NULL,
  `hour` int(11) NOT NULL,
  `weekday` varchar(10) DEFAULT NULL,
  `count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `socio_age_api_plan`
--

CREATE TABLE `socio_age_api_plan` (
  `id` int(11) NOT NULL,
  `id_import` int(11) DEFAULT NULL,
  `id_from` int(11) NOT NULL,
  `ageGroup` int(11) NOT NULL,
  `occurenceType` int(11) NOT NULL,
  `hour` int(11) NOT NULL,
  `finished` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `socio_age_api_syshist`
--

CREATE TABLE `socio_age_api_syshist` (
  `id_hist` int(11) NOT NULL,
  `oper` char(1) NOT NULL,
  `account` varchar(100) NOT NULL,
  `event_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id` int(11) NOT NULL DEFAULT '0',
  `id_import` int(11) DEFAULT NULL,
  `id_from` int(11) NOT NULL,
  `ageGroup` int(11) NOT NULL,
  `occurenceType` int(11) NOT NULL,
  `hour` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `effective_from` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `effective_to` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status_flag` varchar(1) DEFAULT '0',
  `sysaccount_created` varchar(100) DEFAULT NULL,
  `sysaccount_updated` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `socio_gender_api`
--

CREATE TABLE `socio_gender_api` (
  `id` int(11) NOT NULL,
  `id_import` int(11) DEFAULT NULL,
  `id_from` int(11) NOT NULL,
  `gender` int(11) NOT NULL,
  `occurenceType` int(11) NOT NULL,
  `hour` int(11) NOT NULL,
  `weekday` int(10) DEFAULT NULL,
  `count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `socio_gender_api_plan`
--

CREATE TABLE `socio_gender_api_plan` (
  `id` int(11) NOT NULL,
  `id_import` int(11) DEFAULT NULL,
  `id_from` int(11) NOT NULL,
  `gender` int(11) NOT NULL,
  `occurenceType` int(11) NOT NULL,
  `hour` int(11) NOT NULL,
  `finished` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `socio_gender_api_syshist`
--

CREATE TABLE `socio_gender_api_syshist` (
  `id_hist` int(11) NOT NULL,
  `oper` char(1) NOT NULL,
  `account` varchar(100) NOT NULL,
  `event_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id` int(11) NOT NULL DEFAULT '0',
  `id_import` int(11) DEFAULT NULL,
  `id_from` int(11) NOT NULL,
  `gender` int(11) NOT NULL,
  `occurenceType` int(11) NOT NULL,
  `hour` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `effective_from` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `effective_to` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status_flag` varchar(1) DEFAULT '0',
  `sysaccount_created` varchar(100) DEFAULT NULL,
  `sysaccount_updated` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sys_age_group`
--

CREATE TABLE `sys_age_group` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sys_fromtotype`
--

CREATE TABLE `sys_fromtotype` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sys_gender`
--

CREATE TABLE `sys_gender` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sys_hour`
--

CREATE TABLE `sys_hour` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sys_occurence`
--

CREATE TABLE `sys_occurence` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sys_regions`
--

CREATE TABLE `sys_regions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `county_id` varchar(255) DEFAULT NULL,
  `county_name` varchar(255) DEFAULT NULL,
  `mesto` varchar(255) DEFAULT NULL,
  `okres` varchar(255) DEFAULT NULL,
  `kraj` varchar(255) DEFAULT NULL,
  `custom` varchar(255) DEFAULT NULL,
  `lon` varchar(255) DEFAULT NULL,
  `lat` varchar(255) DEFAULT NULL,
  `r_min` varchar(255) DEFAULT NULL,
  `r_eff` varchar(255) DEFAULT NULL,
  `r_max` varchar(255) DEFAULT NULL,
  `radius` varchar(255) NOT NULL DEFAULT '999',
  `note` varchar(1500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sys_uniques`
--

CREATE TABLE `sys_uniques` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sys_zjs_brno`
--

CREATE TABLE `sys_zjs_brno` (
  `id` int(11) NOT NULL,
  `kod_zjs` varchar(50) NOT NULL,
  `nazev_zjs` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sys_zjs_praha`
--

CREATE TABLE `sys_zjs_praha` (
  `id` int(11) NOT NULL,
  `kod_zjs` varchar(50) NOT NULL,
  `nazev_zjs` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_mobility_regions`
-- (See below for the actual view)
--
CREATE TABLE `view_mobility_regions` (
`id` int(11)
,`id_import` int(11)
,`id_from` int(11)
,`region_from` varchar(255)
,`from_lon` varchar(255)
,`from_lat` varchar(255)
,`id_to` int(11)
,`region_to` varchar(255)
,`to_lon` varchar(255)
,`to_lat` varchar(255)
,`uniques` int(11)
,`uniques_names` varchar(255)
,`fromType` int(11)
,`fromType_name` varchar(255)
,`toType` int(11)
,`toType_name` varchar(255)
,`count` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_prehrada_vikend`
-- (See below for the actual view)
--
CREATE TABLE `view_prehrada_vikend` (
`id_from` int(11)
,`name` varchar(255)
,`ageGroup` int(11)
,`occurenceType` int(11)
,`hour` int(11)
,`weekday` varchar(10)
,`count` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_socio_age`
-- (See below for the actual view)
--
CREATE TABLE `view_socio_age` (
`id` int(11)
,`id_import` int(11)
,`id_from` int(11)
,`region_from` varchar(255)
,`lon` varchar(255)
,`lat` varchar(255)
,`age_group_id` int(11)
,`age_group` varchar(255)
,`occurenceType` varchar(7)
,`hour` int(11)
,`count` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_socio_age_2`
-- (See below for the actual view)
--
CREATE TABLE `view_socio_age_2` (
`id` int(11)
,`id_import` int(11)
,`id_from` int(11)
,`region_from` varchar(255)
,`county_from` varchar(255)
,`lon` varchar(255)
,`lat` varchar(255)
,`age_group_id` int(11)
,`age_group` varchar(255)
,`occurenceType` varchar(7)
,`hour` int(11)
,`count` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_socio_gender`
-- (See below for the actual view)
--
CREATE TABLE `view_socio_gender` (
`id` int(11)
,`id_import` int(11)
,`id_from` int(11)
,`region_from` varchar(255)
,`lon` varchar(255)
,`lat` varchar(255)
,`gender` int(11)
,`gender_name` varchar(255)
,`occurenceType` varchar(7)
,`hour` int(11)
,`count` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_socio_gender_2`
-- (See below for the actual view)
--
CREATE TABLE `view_socio_gender_2` (
`id` int(11)
,`id_import` int(11)
,`id_from` int(11)
,`region_from` varchar(255)
,`county_from` varchar(255)
,`lon` varchar(255)
,`lat` varchar(255)
,`gender` int(11)
,`gender_name` varchar(255)
,`occurenceType` varchar(7)
,`hour` int(11)
,`count` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `zsys_uniquezsj_age`
-- (See below for the actual view)
--
CREATE TABLE `zsys_uniquezsj_age` (
`id_from` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `zsys_uniquezsj_gender`
-- (See below for the actual view)
--
CREATE TABLE `zsys_uniquezsj_gender` (
`id_from` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `zsys_uniquezsj_mobility`
-- (See below for the actual view)
--
CREATE TABLE `zsys_uniquezsj_mobility` (
`id_to` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `ZZ_SYS_JOBS_STATUS`
-- (See below for the actual view)
--
CREATE TABLE `ZZ_SYS_JOBS_STATUS` (
`Jobs status` varchar(11)
,`MOBILITY` bigint(21)
,`AGE` bigint(21)
,`GENDER` bigint(21)
);

-- --------------------------------------------------------

--
-- Structure for view `pivot_age_analysis`
--
DROP TABLE IF EXISTS `pivot_age_analysis`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `pivot_age_analysis`  AS  select `mat`.`id_from` AS `region_id`,(select `regions`.`name` from `sys_regions` `regions` where (`regions`.`id` = `mat`.`id_from`)) AS `region_name`,(select `reglon`.`lon` from `sys_regions` `reglon` where (`reglon`.`id` = `mat`.`id_from`)) AS `lon`,(select `reglat`.`lat` from `sys_regions` `reglat` where (`reglat`.`id` = `mat`.`id_from`)) AS `lat`,(select sum(`subq`.`count`) from `socio_age_api` `subq` where ((`mat`.`id_from` = `subq`.`id_from`) and (`subq`.`ageGroup` = 1) and (`subq`.`occurenceType` = 2))) AS `AGE_8_18`,(select sum(`subq`.`count`) from `socio_age_api` `subq` where ((`mat`.`id_from` = `subq`.`id_from`) and (`subq`.`ageGroup` = 2) and (`subq`.`occurenceType` = 2))) AS `AGE_19_25`,(select sum(`subq`.`count`) from `socio_age_api` `subq` where ((`mat`.`id_from` = `subq`.`id_from`) and (`subq`.`ageGroup` = 3) and (`subq`.`occurenceType` = 2))) AS `AGE_26_35`,(select sum(`subq`.`count`) from `socio_age_api` `subq` where ((`mat`.`id_from` = `subq`.`id_from`) and (`subq`.`ageGroup` = 4) and (`subq`.`occurenceType` = 2))) AS `AGE_36_55`,(select sum(`subq`.`count`) from `socio_age_api` `subq` where ((`mat`.`id_from` = `subq`.`id_from`) and (`subq`.`ageGroup` = 5) and (`subq`.`occurenceType` = 2))) AS `AGE_56_PLUS`,(select sum(`subq`.`count`) from `socio_age_api` `subq` where ((`mat`.`id_from` = `subq`.`id_from`) and (`subq`.`occurenceType` = 1))) AS `ALL_TRANZIT` from `zsys_uniquezsj_age` `mat` ;

-- --------------------------------------------------------

--
-- Structure for view `pivot_gender_analysis`
--
DROP TABLE IF EXISTS `pivot_gender_analysis`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `pivot_gender_analysis`  AS  select `mat`.`id_from` AS `region_id`,(select `regions`.`name` from `sys_regions` `regions` where (`regions`.`id` = `mat`.`id_from`)) AS `region_name`,(select `reglon`.`lon` from `sys_regions` `reglon` where (`reglon`.`id` = `mat`.`id_from`)) AS `lon`,(select `reglat`.`lat` from `sys_regions` `reglat` where (`reglat`.`id` = `mat`.`id_from`)) AS `lat`,(select sum(`subq`.`count`) from `socio_gender_api` `subq` where ((`mat`.`id_from` = `subq`.`id_from`) and (`subq`.`gender` = 1) and (`subq`.`occurenceType` = 2))) AS `MALE_VISIT`,(select sum(`subq`.`count`) from `socio_gender_api` `subq` where ((`mat`.`id_from` = `subq`.`id_from`) and (`subq`.`gender` = 2) and (`subq`.`occurenceType` = 2))) AS `FEMALE_VISIT`,(select sum(`subq`.`count`) from `socio_gender_api` `subq` where ((`mat`.`id_from` = `subq`.`id_from`) and (`subq`.`gender` = 1) and (`subq`.`occurenceType` = 1))) AS `MALE_TRANZIT`,(select sum(`subq`.`count`) from `socio_gender_api` `subq` where ((`mat`.`id_from` = `subq`.`id_from`) and (`subq`.`gender` = 2) and (`subq`.`occurenceType` = 1))) AS `FEMALE_TRANZIT` from `zsys_uniquezsj_gender` `mat` ;

-- --------------------------------------------------------

--
-- Structure for view `pivot_mobility_analysis`
--
DROP TABLE IF EXISTS `pivot_mobility_analysis`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `pivot_mobility_analysis`  AS  select `mat`.`id_to` AS `region_id`,(select `regions`.`name` from `sys_regions` `regions` where (`regions`.`id` = `mat`.`id_to`)) AS `region_name`,(select `reglon`.`lon` from `sys_regions` `reglon` where (`reglon`.`id` = `mat`.`id_to`)) AS `lon`,(select `reglat`.`lat` from `sys_regions` `reglat` where (`reglat`.`id` = `mat`.`id_to`)) AS `lat`,(select sum(`subq`.`count`) from `mobility_api` `subq` where ((`mat`.`id_to` = `subq`.`id_to`) and (`subq`.`toType` = 2) and (`subq`.`uniques` = 1))) AS `TO_UNIQUE_VISITS`,(select sum(`subq`.`count`) from `mobility_api` `subq` where ((`mat`.`id_to` = `subq`.`id_to`) and (`subq`.`toType` = 1) and (`subq`.`uniques` = 1))) AS `TO_UNIQUE_TRANZITS`,(select sum(`subq`.`count`) from `mobility_api` `subq` where ((`mat`.`id_to` = `subq`.`id_to`) and (`subq`.`toType` = 0) and (`subq`.`uniques` = 1))) AS `TO_UNIQUE_BOTH` from `zsys_uniquezsj_mobility` `mat` ;

-- --------------------------------------------------------

--
-- Structure for view `view_mobility_regions`
--
DROP TABLE IF EXISTS `view_mobility_regions`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_mobility_regions`  AS  select `mobility`.`id` AS `id`,`mobility`.`id_import` AS `id_import`,`mobility`.`id_from` AS `id_from`,(select `regions`.`name` from `sys_regions` `regions` where (`regions`.`id` = `mobility`.`id_from`)) AS `region_from`,(select `reglon`.`lon` from `sys_regions` `reglon` where (`reglon`.`id` = `mobility`.`id_from`)) AS `from_lon`,(select `reglat`.`lat` from `sys_regions` `reglat` where (`reglat`.`id` = `mobility`.`id_from`)) AS `from_lat`,`mobility`.`id_to` AS `id_to`,(select `regions`.`name` from `sys_regions` `regions` where (`regions`.`id` = `mobility`.`id_to`)) AS `region_to`,(select `reglon`.`lon` from `sys_regions` `reglon` where (`reglon`.`id` = `mobility`.`id_to`)) AS `to_lon`,(select `reglat`.`lat` from `sys_regions` `reglat` where (`reglat`.`id` = `mobility`.`id_to`)) AS `to_lat`,`mobility`.`uniques` AS `uniques`,(select `uni`.`name` from `sys_uniques` `uni` where (`uni`.`id` = `mobility`.`uniques`)) AS `uniques_names`,`mobility`.`fromType` AS `fromType`,(select `FTT`.`name` from `sys_fromtotype` `FTT` where (`FTT`.`id` = `mobility`.`fromType`)) AS `fromType_name`,`mobility`.`toType` AS `toType`,(select `FTT`.`name` from `sys_fromtotype` `FTT` where (`FTT`.`id` = `mobility`.`toType`)) AS `toType_name`,`mobility`.`count` AS `count` from `mobility_api` `mobility` ;

-- --------------------------------------------------------

--
-- Structure for view `view_prehrada_vikend`
--
DROP TABLE IF EXISTS `view_prehrada_vikend`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vilem`@`%` SQL SECURITY DEFINER VIEW `view_prehrada_vikend`  AS  select `A`.`id_from` AS `id_from`,`R`.`name` AS `name`,`A`.`ageGroup` AS `ageGroup`,`A`.`occurenceType` AS `occurenceType`,`A`.`hour` AS `hour`,`A`.`weekday` AS `weekday`,`A`.`count` AS `count` from (`socio_age_api` `A` join `sys_regions` `R` on((`A`.`id_from` = `R`.`id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_socio_age`
--
DROP TABLE IF EXISTS `view_socio_age`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_socio_age`  AS  select `age`.`id` AS `id`,`age`.`id_import` AS `id_import`,`age`.`id_from` AS `id_from`,(select `regions`.`name` from `sys_regions` `regions` where (`regions`.`id` = `age`.`id_from`)) AS `region_from`,(select `reglon`.`lon` from `sys_regions` `reglon` where (`reglon`.`id` = `age`.`id_from`)) AS `lon`,(select `reglat`.`lat` from `sys_regions` `reglat` where (`reglat`.`id` = `age`.`id_from`)) AS `lat`,`age`.`ageGroup` AS `age_group_id`,(select `agegroup`.`name` from `sys_age_group` `agegroup` where (`agegroup`.`id` = `age`.`ageGroup`)) AS `age_group`,(case `age`.`occurenceType` when 1 then 'TRANZIT' when 2 then 'VISIT' end) AS `occurenceType`,`age`.`hour` AS `hour`,`age`.`count` AS `count` from `socio_age_api` `age` ;

-- --------------------------------------------------------

--
-- Structure for view `view_socio_age_2`
--
DROP TABLE IF EXISTS `view_socio_age_2`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vilem`@`%` SQL SECURITY DEFINER VIEW `view_socio_age_2`  AS  select `age`.`id` AS `id`,`age`.`id_import` AS `id_import`,`age`.`id_from` AS `id_from`,(select `regions`.`name` from `sys_regions` `regions` where (`regions`.`id` = `age`.`id_from`)) AS `region_from`,(select `regions`.`county_name` from `sys_regions` `regions` where (`regions`.`id` = `age`.`id_from`)) AS `county_from`,(select `reglon`.`lon` from `sys_regions` `reglon` where (`reglon`.`id` = `age`.`id_from`)) AS `lon`,(select `reglat`.`lat` from `sys_regions` `reglat` where (`reglat`.`id` = `age`.`id_from`)) AS `lat`,`age`.`ageGroup` AS `age_group_id`,(select `agegroup`.`name` from `sys_age_group` `agegroup` where (`agegroup`.`id` = `age`.`ageGroup`)) AS `age_group`,(case `age`.`occurenceType` when 1 then 'TRANZIT' when 2 then 'VISIT' end) AS `occurenceType`,`age`.`hour` AS `hour`,`age`.`count` AS `count` from `socio_age_api` `age` ;

-- --------------------------------------------------------

--
-- Structure for view `view_socio_gender`
--
DROP TABLE IF EXISTS `view_socio_gender`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_socio_gender`  AS  select `gender`.`id` AS `id`,`gender`.`id_import` AS `id_import`,`gender`.`id_from` AS `id_from`,(select `regions`.`name` from `sys_regions` `regions` where (`regions`.`id` = `gender`.`id_from`)) AS `region_from`,(select `reglon`.`lon` from `sys_regions` `reglon` where (`reglon`.`id` = `gender`.`id_from`)) AS `lon`,(select `reglat`.`lat` from `sys_regions` `reglat` where (`reglat`.`id` = `gender`.`id_from`)) AS `lat`,`gender`.`gender` AS `gender`,(select `gendic`.`name` from `sys_gender` `gendic` where (`gendic`.`id` = `gender`.`gender`)) AS `gender_name`,(case `gender`.`occurenceType` when 1 then 'TRANZIT' when 2 then 'VISIT' end) AS `occurenceType`,`gender`.`hour` AS `hour`,`gender`.`count` AS `count` from `socio_gender_api` `gender` ;

-- --------------------------------------------------------

--
-- Structure for view `view_socio_gender_2`
--
DROP TABLE IF EXISTS `view_socio_gender_2`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vilem`@`%` SQL SECURITY DEFINER VIEW `view_socio_gender_2`  AS  select `gender`.`id` AS `id`,`gender`.`id_import` AS `id_import`,`gender`.`id_from` AS `id_from`,(select `regions`.`name` from `sys_regions` `regions` where (`regions`.`id` = `gender`.`id_from`)) AS `region_from`,(select `regions`.`county_name` from `sys_regions` `regions` where (`regions`.`id` = `gender`.`id_from`)) AS `county_from`,(select `reglon`.`lon` from `sys_regions` `reglon` where (`reglon`.`id` = `gender`.`id_from`)) AS `lon`,(select `reglat`.`lat` from `sys_regions` `reglat` where (`reglat`.`id` = `gender`.`id_from`)) AS `lat`,`gender`.`gender` AS `gender`,(select `gendic`.`name` from `sys_gender` `gendic` where (`gendic`.`id` = `gender`.`gender`)) AS `gender_name`,(case `gender`.`occurenceType` when 1 then 'TRANZIT' when 2 then 'VISIT' end) AS `occurenceType`,`gender`.`hour` AS `hour`,`gender`.`count` AS `count` from `socio_gender_api` `gender` ;

-- --------------------------------------------------------

--
-- Structure for view `zsys_uniquezsj_age`
--
DROP TABLE IF EXISTS `zsys_uniquezsj_age`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `zsys_uniquezsj_age`  AS  select `socio_age_api`.`id_from` AS `id_from` from `socio_age_api` group by `socio_age_api`.`id_from` ;

-- --------------------------------------------------------

--
-- Structure for view `zsys_uniquezsj_gender`
--
DROP TABLE IF EXISTS `zsys_uniquezsj_gender`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `zsys_uniquezsj_gender`  AS  select `socio_gender_api`.`id_from` AS `id_from` from `socio_gender_api` group by `socio_gender_api`.`id_from` ;

-- --------------------------------------------------------

--
-- Structure for view `zsys_uniquezsj_mobility`
--
DROP TABLE IF EXISTS `zsys_uniquezsj_mobility`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `zsys_uniquezsj_mobility`  AS  select `mobility_api`.`id_to` AS `id_to` from `mobility_api` group by `mobility_api`.`id_to` ;

-- --------------------------------------------------------

--
-- Structure for view `ZZ_SYS_JOBS_STATUS`
--
DROP TABLE IF EXISTS `ZZ_SYS_JOBS_STATUS`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `ZZ_SYS_JOBS_STATUS`  AS  select 'Jobs status' AS `Jobs status`,(select count(0) from `mobility_api_plan` where (`mobility_api_plan`.`finished` = 0)) AS `MOBILITY`,(select count(0) from `socio_age_api_plan` where (`socio_age_api_plan`.`finished` = 0)) AS `AGE`,(select count(0) from `socio_gender_api_plan` where (`socio_gender_api_plan`.`finished` = 0)) AS `GENDER` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `import_log`
--
ALTER TABLE `import_log`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `mobility_api`
--
ALTER TABLE `mobility_api`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_from` (`id_from`),
  ADD KEY `id_to` (`id_to`);

--
-- Indexes for table `mobility_api_plan`
--
ALTER TABLE `mobility_api_plan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `mobility_api_syshist`
--
ALTER TABLE `mobility_api_syshist`
  ADD PRIMARY KEY (`id_hist`);

--
-- Indexes for table `socio_age_api`
--
ALTER TABLE `socio_age_api`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_from` (`id_from`),
  ADD KEY `ageGroup` (`ageGroup`);

--
-- Indexes for table `socio_age_api_plan`
--
ALTER TABLE `socio_age_api_plan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `socio_age_api_syshist`
--
ALTER TABLE `socio_age_api_syshist`
  ADD PRIMARY KEY (`id_hist`);

--
-- Indexes for table `socio_gender_api`
--
ALTER TABLE `socio_gender_api`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_from` (`id_from`),
  ADD KEY `gender` (`gender`);

--
-- Indexes for table `socio_gender_api_plan`
--
ALTER TABLE `socio_gender_api_plan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `socio_gender_api_syshist`
--
ALTER TABLE `socio_gender_api_syshist`
  ADD PRIMARY KEY (`id_hist`);

--
-- Indexes for table `sys_age_group`
--
ALTER TABLE `sys_age_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- Indexes for table `sys_fromtotype`
--
ALTER TABLE `sys_fromtotype`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- Indexes for table `sys_gender`
--
ALTER TABLE `sys_gender`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- Indexes for table `sys_hour`
--
ALTER TABLE `sys_hour`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- Indexes for table `sys_occurence`
--
ALTER TABLE `sys_occurence`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- Indexes for table `sys_regions`
--
ALTER TABLE `sys_regions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `ctvrt` (`county_id`),
  ADD KEY `mesto` (`mesto`),
  ADD KEY `okres` (`okres`),
  ADD KEY `kraj` (`kraj`),
  ADD KEY `custom` (`custom`),
  ADD KEY `radius` (`radius`),
  ADD KEY `name` (`name`),
  ADD KEY `county_name` (`county_name`);

--
-- Indexes for table `sys_uniques`
--
ALTER TABLE `sys_uniques`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- Indexes for table `sys_zjs_brno`
--
ALTER TABLE `sys_zjs_brno`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kod_zjs` (`kod_zjs`),
  ADD KEY `nazev_zjs` (`nazev_zjs`);

--
-- Indexes for table `sys_zjs_praha`
--
ALTER TABLE `sys_zjs_praha`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kod_zjs` (`kod_zjs`),
  ADD KEY `nazev_zjs` (`nazev_zjs`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `import_log`
--
ALTER TABLE `import_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mobility_api`
--
ALTER TABLE `mobility_api`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58691;
--
-- AUTO_INCREMENT for table `mobility_api_plan`
--
ALTER TABLE `mobility_api_plan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88753;
--
-- AUTO_INCREMENT for table `mobility_api_syshist`
--
ALTER TABLE `mobility_api_syshist`
  MODIFY `id_hist` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `socio_age_api`
--
ALTER TABLE `socio_age_api`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47747;
--
-- AUTO_INCREMENT for table `socio_age_api_plan`
--
ALTER TABLE `socio_age_api_plan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81961;
--
-- AUTO_INCREMENT for table `socio_age_api_syshist`
--
ALTER TABLE `socio_age_api_syshist`
  MODIFY `id_hist` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `socio_gender_api`
--
ALTER TABLE `socio_gender_api`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27785;
--
-- AUTO_INCREMENT for table `socio_gender_api_plan`
--
ALTER TABLE `socio_gender_api_plan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32785;
--
-- AUTO_INCREMENT for table `socio_gender_api_syshist`
--
ALTER TABLE `socio_gender_api_syshist`
  MODIFY `id_hist` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sys_age_group`
--
ALTER TABLE `sys_age_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sys_gender`
--
ALTER TABLE `sys_gender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sys_hour`
--
ALTER TABLE `sys_hour`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `sys_occurence`
--
ALTER TABLE `sys_occurence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sys_regions`
--
ALTER TABLE `sys_regions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=392392;
--
-- AUTO_INCREMENT for table `sys_uniques`
--
ALTER TABLE `sys_uniques`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sys_zjs_brno`
--
ALTER TABLE `sys_zjs_brno`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=493;
--
-- AUTO_INCREMENT for table `sys_zjs_praha`
--
ALTER TABLE `sys_zjs_praha`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=535;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
