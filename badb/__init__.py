# -*- coding: utf-8 -*-
import sys  
reload(sys)  
sys.setdefaultencoding('utf8')
import flask
import os
import sqlalchemy
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import NullPool

APP = flask.Flask(__name__)

#APP.config.from_object("badb.default_config")
if "BADB_CONFIG" in os.environ: # pragma: no cover
    APP.config.from_envvar("BADB_CONFIG")


def connect_db(dbname):
    db = "DB_{0}".format(dbname.upper())

    if APP.config[db].startswith("mysql:"):
        args = {"charset": "utf8", "use_unicode": 1}
    else:
        args = {}

    engine = sqlalchemy.create_engine(APP.config[db], echo=False, connect_args=args, poolclass=NullPool)
    Session = sessionmaker(bind=engine, autoflush=False, autocommit=False)
    session = Session()

    return session

def get_db(dbname):
    db = getattr(flask.g, "_database_{0}".format(dbname), None)
    if db is None:
        db = connect_db(dbname)
        setattr(flask.g, "_database_{0}".format(dbname), db)
    return db

@APP.teardown_appcontext
def close_connection(exception):
    for attr in dir(flask.g):
        if attr.startswith("_database_"):
            db = getattr(flask.g, attr, None)
            if db is not None:
                db.close()

@APP.route("/get_regions")
def get_regions():
    from badb.liberty import get_regions
    return flask.json.jsonify(regions=get_regions())  

@APP.route("/get_age_by_time/<int:region_id>/<type>")
def get_age_by_time(region_id, type):
    from badb.liberty import get_age_by_time
    return flask.json.jsonify(**get_age_by_time(region_id, type))  

@APP.route("/get_mobility/<int:region_id>/<type>")
def get_mobility(region_id, type):
    from badb.liberty import get_mobility
    return flask.json.jsonify(**get_mobility(region_id, type))  

@APP.route("/get_gender/<int:region_id>")
def get_gender(region_id):
    from badb.liberty import get_gender
    return flask.json.jsonify(**get_gender(region_id))  
