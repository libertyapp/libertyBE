from badb import APP

def mysql_connect():
    import MySQLdb
    conn = MySQLdb.connect(**APP.config["DB_LIBERTY"])
    liberty = conn.cursor ()
    return {"conn":conn, "liberty":liberty}

TEMPLATE_REGIONS = '''
SELECT name, lon, lat, radius, id FROM dev_hathl_liberty.sys_regions
where radius < 5 
'''

TEMPLATE_AGE_BY_TIME = '''
SELECT saa.count, sh.name as hour_label, sr.name as region, sag.name as age, so.name as occurence, saa.hour FROM socio_age_api saa
left join sys_hour sh on saa.hour = sh.id
left join sys_regions sr on saa.id_from = sr.id
left join sys_age_group sag on saa.ageGroup = sag.id
left join sys_occurence so on saa.occurenceType = so.id
where id_from = {0}
and weekday = "{1}"
order by saa.hour
'''

TEMPLATE_GET_MOBILITY_FROM = '''
SELECT ma.count, ma.id_from as point_from, ma.id_to as point_to, sr_from.name as label_from, sr_to.name as label_to, ma.distance FROM mobility_api ma
left join sys_regions sr_from on ma.id_from = sr_from.id
left join sys_regions sr_to on ma.id_to = sr_to.id
where id_from = {0}
and uniques = 1
and fromtype = 1
and totype = 0
and ma.distance <> 0
order by ma.distance
'''

TEMPLATE_GET_MOBILITY_FROM_OBCD = '''
SELECT ma.count, ma.id_from as point_from, ma.id_to as point_to, sr_from.name as label_from, sr_to.name as label_to, ma.distance FROM mobility_api ma
left join sys_regions sr_from on ma.id_from = sr_from.id
left join sys_regions sr_to on ma.id_to = sr_to.id
where id_from = {0}
and uniques = 1
and fromtype = 1
and totype = 0
and ma.distance <> 0
order by ma.count desc
'''

TEMPLATE_GET_MOBILITY_TO = '''
SELECT ma.count, ma.id_from as point_from, ma.id_to as point_to, sr_from.name as label_from, sr_to.name as label_to, ma.distance FROM mobility_api ma
left join sys_regions sr_from on ma.id_from = sr_from.id
left join sys_regions sr_to on ma.id_to = sr_to.id
where id_to = {0}
and uniques = 1
and fromtype = 1
and totype = 0
and ma.distance <> 0
'''

TEMPLATE_GET_GENDER = '''
SELECT sg.count, sg.hour, gender.name as gender FROM socio_gender_api sg 
left join sys_gender gender on sg.gender = gender.id
where id_from = {0} 
'''

TEMPLATE_GET_AGE_GROUPS = '''
SELECT name FROM sys_age_group
order by id
'''


def get_gender(region_id):
    my = mysql_connect()
    liberty = my["liberty"]
    conn = my["conn"]
    liberty.execute(TEMPLATE_GET_GENDER.format(region_id))
    result = liberty.fetchall()

    data = []
    for row in result:
        count = {}
        count["count"] = row[0]
        count["hour"] = row[1]
        count["gender"] = row[2]
        data.append(count)
    
    genders = {"FEMALE":{}, "MALE":{}}
    for d in data:
        if d["gender"] == "FEMALE":
            genders[d["gender"]][d["hour"]] = 0 - d["count"]
        else:
            genders[d["gender"]][d["hour"]] =  d["count"]

    values = []
    final = []
    for f in genders["FEMALE"].keys():
        values.append({"label":str(f), "value":genders["FEMALE"][f]})
    final.append({"key":"female", "color":"#d62728", "values":values[:]}) 
    values = [] 
    for f in genders["MALE"].keys():
        values.append({"label":str(f), "value":genders["MALE"][f]})
    final.append({"key":"male", "color":"#1f77b4", "values":values[:]}) 
    
    liberty.close()
    conn.close()
    return {"data":final}


def get_mobility(region_id, type):
    my = mysql_connect()
    liberty = my["liberty"]
    conn = my["conn"]
    if type == "distance":
        liberty.execute(TEMPLATE_GET_MOBILITY_FROM.format(region_id))
    else:
        liberty.execute(TEMPLATE_GET_MOBILITY_FROM_OBCD.format(region_id))

    result = liberty.fetchall()

    data = []
    for row in result:
        count = {}
        count["count"] = row[0]
        count["point_from"] = row[1]
        count["point_to"] = row[2]
        count["label_from"] = row[3]
        count["label_to"] = row[4]
        count["distance"] = row[5]
        data.append(count)

    regions = []
    region_labels = [] 
    region_distances = [] 
    dist = []
    
    for d in data:
        if d["point_to"] not in regions:
            regions.append(d["point_to"])
            region_labels.append(d["label_to"])
            region_distances.append(d["distance"])
        dist.append({"x":regions.index(d["point_to"]), "y":d["count"]})      

    liberty.execute(TEMPLATE_GET_MOBILITY_TO.format(region_id))
    result = liberty.fetchall()

    data = []
    for row in result:
        count = {}
        count["count"] = row[0]
        count["point_from"] = row[1]
        count["point_to"] = row[2]
        count["label_from"] = row[3]
        count["label_to"] = row[4]
        data.append(count)

    dist_in = []
    
    for d in data:
        #if d["point_to"] not in regions:
        #    regions.append(d["point_from"])
        #dist_in.append({"x":regions.index(d["point_from"]), "y":d["count"]})      
        if d["point_from"] in regions:
            dist_in.append({"x":regions.index(d["point_from"]), "y":d["count"]})      
    import operator
    dist_in.sort(key=operator.itemgetter("x"))

    liberty.close()
    conn.close()
    return {"data":[{"key":"out","values":dist},{"key":"in","values":dist_in}], "labels":region_labels, "distances":region_distances}
    #[{"x":1,"y":2},{"x":2,"y":4}]



def get_age_by_time(region_id, type):
    my = mysql_connect()
    liberty = my["liberty"]
    conn = my["conn"]
    liberty.execute(TEMPLATE_GET_AGE_GROUPS)
    result = liberty.fetchall()
    age_groups = []
    for row in result:
        age_groups.append(row[0])
    liberty.execute(TEMPLATE_AGE_BY_TIME.format(region_id, type))
    result = liberty.fetchall()

    data = []
    for row in result:
        count = {}
        count["count"] = row[0]
        count["hour_label"] = row[1]
        count["region"] = row[2]
        count["age"] = row[3]
        count["occurence"] = row[4]
        count["hour"] = row[5]
        data.append(count)

    final = {"data":[]}

    points = {}
    for d in data:
        if d["age"] not in points:
            points[d["age"]] = []
        for i in range(len(points[d["age"]]), d["hour"]):
            points[d["age"]].append([i, 0])
        points[d["age"]].append([d["hour"], d["count"]])
    #for p in points.keys():
    #        for i in range(len(points[p]), 24):
    #            points[p].append([i,0])
    #        final["data"].append({"key":p, "values":points[p]})
    for p in points.keys():
            for i in range(len(points[p]), 24):
                points[p].append([i,0])
            final["data"].append({"key":p, "values":points[p]})
            age_groups.remove(p)
    zeros = []
    for i in range(0,24):
        zeros.append([i, 0])
    for ag in age_groups:
        final["data"].append({"key":ag,"values":zeros})
    import operator
    final["data"].sort(key=operator.itemgetter("key"))
    liberty.close()
    conn.close()
    return final 

def get_regions():
    my = mysql_connect()
    liberty = my["liberty"]
    conn = my["conn"]
    liberty.execute(TEMPLATE_REGIONS)
    result = liberty.fetchall()
     
    regions = []
    for row in result:
        region = {}
        region['name'] = row[0]
        region['lon'] = row[1]
        region['lat'] = row[2]
        region['radius'] = row[3] 
        region['region_id'] = row[4] 
        regions.append(region)
    liberty.close()
    conn.close()
    return regions
